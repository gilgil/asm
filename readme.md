# Samsung CTF Asm Write-up

### url

[https://sctf.codeground.org/ctf/](https://sctf.codeground.org/ctf/) > CTF > Coding/Asm

### problem

```
Addition, Subtraction, Multiplication.

Toooooooooo e3sy to calculate!

I believe you can solve 100 problems in 80 seconds:P

http://asm.eatpwnnosleep.com/
```

### 문제 풀이

http://asm.eatpwnnosleep.com/ 사이트에 들어 가서 Start를 누르면 문자와 숫자로 구성이 된 그림이 보인다. 이 그림에 있는 글자를 수식으로 계산하여 그 정답을 기입하면 다음 그림이 나오게 되고, 이 과정을 100번 하게 되면 flag가 보이게 된다. 주어진 시간(80초) 이내에 100 문제를 풀어야 하는데, 가면 갈 수록 (사람이 하기에는) 계산하는 데에 시간이 오래 걸리게 되므로 이런 일련의 과정을 사람이 아닌 자동으로 할 수 있는 코딩을 작성하는 것이 문제이다.

### 풀이 순서

png file 수집

* 몇 번의 수작업 시도로 인해 그림 파일(png format)들을 모은다.
* https://gitlab.com/gilgil/asm/tree/master/pcap
* 웹브라우징을 통해 얻은 pcap file에서 png file을 추출한다.
* https://gitlab.com/gilgil/asm/tree/master/pcap/asm1
* 이 작업을 여러번 하여 png file들을 추출한다.

png file 분석

* 사용되어 진 문자는 0~9, +, -, * 의 문자로 구성이 된다.
* 같은 문자에 대해 크기는 동일하며 색깔은 RGB 각각 (255, 255, 170), (255, 170, 255), (170, 255, 255)이다.
* 이는 png file에서 추출된 이차원 이미지를 분석하면 문자를 쉽게 추출해 낼 수 있다는 결론을 내렸다. 단 anti-aliasing이 적용되어 있어서 이 부분을 처리하는 데 신경을 써 줘야 한다.
* 각 문자를 이미지 편집 툴로 하나씩 빼 낸다.
* https://gitlab.com/gilgil/asm/tree/master/qt/bin/char

http 통신

* 수집된 pcap file을 통해 http 통신의 protocol을 분석한다.
* Qt에서 제공해 주는 QNetworkAccessManager, QNetworkRequest, QNetworkReply 클래스를 이용하여 start(시작), getImage(이미지 가져 오기), check(정답 제출) 등을 구현한다.
* https://gitlab.com/gilgil/asm/blob/master/qt/asm/http.h
* https://gitlab.com/gilgil/asm/blob/master/qt/asm/http.cpp

문자 인식

* 시간이 가장 오래 걸렸던 부분이기는 하지만, 이미지 패턴이 고정적이어서 그리 어렵지는 않았다. 단 이미지가 이차원 배열인 지라 Qt에서 제공해 주는 클래스(QImage)를 이용하여 픽셀 정보를 얻어 왔는데, 이것이 상당히 느릭 작동을 한다(다양한 color space를 지원해야 해서). 그래서 결국 CaptchaImage라는 별도의 클래스를 만들어서 2차원 픽셀을 관리할 수 있는 클래스를 만들었다.
* 이미지가 같은지 인식을 하기 위해서는 왼쪽에서부터 오른쪽으로 스캔을 해야 한다. 이를 위해서 for문을 돌 때 x를 바깥 loop으로, y를 안쪽 loop으로 돌게 하고, 또한 이차원 배열에서 x, y 좌표의 인덱스를 얻는 과정에서 "x * height_ + y"라는 수식어를 통해 cache miss를 방지할 수 있도록 하였다.
* https://gitlab.com/gilgil/asm/blob/master/qt/asm/captcha.h
* https://gitlab.com/gilgil/asm/blob/master/qt/asm/captcha.cpp

계산

* 별거 없다. operand1, operator, operand1 빼 내서 계산해 주면 됨.
* https://gitlab.com/gilgil/asm/blob/master/qt/asm/parser.h

큰 수 처리

* 처음에는 작은 숫자들아 나오는데 나중에 가서는 32bit, 64bit 정수를 벗어나는 숫자들이 보이기 시작했다(멘붕).
* 이를 위해 C++에서 Big Integer를 지원하는 클래스를 검색을 통하여 추가하였다.
* https://gitlab.com/gilgil/asm/tree/master/qt/asm/bigint

실행 결과

* https://gitlab.com/gilgil/asm/blob/master/Screencast_07-09-2017_02:57:02%20PM.webm

### 문제 풀이 소감

* 본 문에는 다음과 같은 기능을 구현하는 것을 요구하고 있다.
  * 네트워크 통신 : HTTP 통신 라이브러리 사용 능력 요구.
  * 문자 인식 : png file에서 이미지를 불러 와서 이차원 배열에 추가한 다음 처리할 수 있는 능력 요구.
  * 숫자 처리 : Big Integer를 처리할 수 있는 능력 요구.

* 기존의 google code jam이나 acm-icpc와 같은 기존의 contest와는 다르게 신선한 방식을 요구하고 있어서 재미있게 문제를 풀어 본 것 같다. 코딩을 많이 해 본 사람에게 유리한 문제로 판단됨.

* 문제 풀이에 60초 정도가 걸렸는데, PC의 성능이나 네트워크 상태에 따라 100문제를 80초 안에 푸는 것이 불가능한 경우가 있지는 않을까 하는 우려를 해 봄.

* 애 딸린 유뷰남이 주말에 이런 대회에 참가하는 것은 역시나 힘이 들구나 하는 것을 또 한번 느끼게 됨.

* 마지막으로 좋은 문제 내 주셔서 감사합니다. ^^

### References
* [김민철 풀이](minchul.kim.5688.mp4) ( https://www.facebook.com/minchul.kim.5688/videos/1355850001151169 )
