#include "http.h"

Http::Http()
{

}

bool Http::start() {
  QEventLoop eventLoop;
  QObject::connect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

  QNetworkRequest req( QUrl( QString("http://asm.eatpwnnosleep.com/start") ) );
  req.setRawHeader("Accept-Encoding", "deflate");
  QByteArray dummy;
  QNetworkReply *reply = mgr_.post(req, dummy);
  eventLoop.exec();

  if (reply->error() != QNetworkReply::NoError) {
    qCritical() << "reply->error() failed" <<reply->errorString();
    delete reply;
    return false;
  }

  uri_ = reply->readAll();
  if (uri_ == "") {
    qCritical() << "reply->readAll is null";
    delete reply;
    return false;
  }

  qDebug() << "uri=" << uri_;
  QObject::disconnect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
  delete reply;
  return true;
}

CaptchaImage Http::getImage() {
  QEventLoop eventLoop;
  QObject::connect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

  QNetworkRequest req( QUrl( QString("http://asm.eatpwnnosleep.com" + uri_) ) );
  QNetworkReply* reply = mgr_.get(req);
  eventLoop.exec();

  if (reply->error() != QNetworkReply::NoError) {
    qCritical() << "reply->error() 2 failed" <<reply->errorString();
    delete reply;
    return CaptchaImage();
  }

  QByteArray png = reply->readAll();
  QPixmap pm;
  if (!pm.loadFromData(png)) {
    qCritical() << "pm.loadFromData return false";
    delete reply;
    return CaptchaImage();
  }

  CaptchaImage img;
  img.load(pm.toImage());
  qDebug() << "image size" << img.width() << img.height(); // gilgil temp
  QObject::disconnect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
  return img;
}

bool Http::check(BigInteger ans) {
  QEventLoop eventLoop;
  QObject::connect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

  QNetworkRequest req( QUrl( QString("http://asm.eatpwnnosleep.com/check") ) );
  req.setRawHeader("Accept-Encoding", "deflate");
  req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded; charset=UTF-8");

  QString p = QString("ans=%1").arg(bigIntegerToString(ans).c_str());
  QByteArray param = qPrintable(p);
  QNetworkReply *reply = mgr_.post(req, param);
  eventLoop.exec();

  if (reply->error() != QNetworkReply::NoError) {
    qCritical() << "reply->error() failed" <<reply->errorString();
    delete reply;
    return false;
  }

  QString recv = reply->readAll();
  static QRegExp rx("/images/[^\"]*");
  int res = rx.indexIn(recv);
  if (res == -1) {
    qCritical() << "can not find uri recv=" << recv;
    return false;
  }
  uri_ = recv.mid(res, rx.matchedLength());
  qDebug() << "uri=" << uri_;
  if (uri_ == "") {
    qCritical() << "reply->readAll is null";
    delete reply;
    return false;
  }
  QObject::disconnect(&mgr_, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
  delete reply;
  return true;
}

QString Http::getPattern(QRegExp rx, QString data, int index) {
  int pos = rx.indexIn(data);
  if (pos == -1) {
    qCritical() << "rx.indexIn return -1";
    return "";
  }
  QStringList list = rx.capturedTexts();
  if (list.count() < index + 1) {
    qCritical() << QString("list.count() is small %1").arg(list.count());
    return "";
  }

  QString res = list[index];
  // qDebug() << res; // gilgil temp
  return res;
}
