#include "parser.h"

Parser::Parser()
{

}

BigInteger Parser::calculate(QString s) {
  int i = 0;

  BigInteger op1 = 0;
  while (true) {
    char c = s[i].toLatin1();
    if (!isdigit(c))
      break;
    op1 = op1 * 10 + (c - '0');
    i++;
  }

  char opr = s[i++].toLatin1();

  std::string _s = qPrintable(s.mid(i));
  BigInteger op2 = stringToBigInteger(_s);

  // qDebug() << op1 << opr << op2; // gilgil temp
  BigInteger res = 0;
  switch (opr) {
  case '+': res = op1 + op2; break;
  case '-': res = op1 - op2; break;
  case '*': res = op1 * op2; break;
  default: qCritical() << QString("invalid operator %1").arg(opr); return 0;
  }
  return res;
}
