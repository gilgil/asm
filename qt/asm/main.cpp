#include <QApplication>
#include <QDebug>

#include "captcha.h"
#include "http.h"
#include "parser.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  Http h;
  Captcha c;
  Parser p;

  if (!h.start()) return 0;
  for (int i = 0; i <= 100; i++) {
    CaptchaImage img = h.getImage();
    QString s = c.solve(img);
    //qDebug() << s; // gilgil temp
    BigInteger ans = p.calculate(s);
    std::clog << i << " answer=" << ans << std::endl; // gilgil temp
    if (!h.check(ans)) {
      qCritical() << QString("answer return false");
      break;
    }
  }
  return 0;
}
