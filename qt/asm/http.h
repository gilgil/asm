#pragma once

#include <QDebug>
#include <QImage>
#include <QEventLoop>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPixmap>

#include "captcha.h"
#include "bigint/BigInteger.hh"
#include "bigint/BigIntegerUtils.hh"

struct Http {
  Http();
  bool start();
  CaptchaImage getImage();
  bool check(BigInteger ans);

protected:
  QString getPattern(QRegExp rx, QString data, int index);
  QNetworkAccessManager mgr_;
  QString cookie_;
  QString uri_;
};
