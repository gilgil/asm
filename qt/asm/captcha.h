#pragma once

#include <QDebug>
#include <QImage>
#include <QList>
#include <QPixmap>

struct CaptchaImage {
  CaptchaImage();
  CaptchaImage(const CaptchaImage& r);
  void load(const QImage& img);
  virtual ~CaptchaImage();
  int width() const { return width_; }
  int height() const { return height_; }
  QRgb pixel(int x, int y) const {
    size_t offset = x * height_ + y;
    QRgb* p = buf_ + offset;
    QRgb rgb = *p;
    return rgb;
  }
  void setPixel(int x, int y, QRgb rgb) {
    size_t offset = x * height_ + y;
    QRgb* p = buf_ + offset;
    *p = rgb;
  }

  void save(QString fileName);

protected:
  int width_{0};
  int height_{0};
  QRgb* buf_{nullptr};
  size_t size_{0};
};

struct CaptchaChar {
  CaptchaImage img_;
  //QImage img_;
  QChar  ch_;
};

typedef enum {
  R,
  G,
  B
} CaptchaColor;

typedef QList<CaptchaChar> CaptchaCharList;

struct Captcha {
  Captcha();
  QString solve(CaptchaImage& target);

protected:
  CaptchaCharList ccList_;

  QChar solve(int x, int y, CaptchaImage& target);
  void load(QString fileName, QChar ch);
  static int max(int a, int b, int c);
  static bool check(const CaptchaImage& img, int x, int y, CaptchaColor c, CaptchaImage& target);
  static void sub(const CaptchaImage& img, int x, int y, CaptchaColor c, CaptchaImage& target);
  static void inverse(CaptchaImage& target);
};
