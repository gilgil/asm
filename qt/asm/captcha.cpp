#include "captcha.h"

CaptchaImage::CaptchaImage() {
}

void CaptchaImage::load(const QImage& img){
  width_ = img.width();
  height_ = img.height();
  size_ = width_ * height_;
  buf_ = new QRgb[size_];
  for (int x = 0; x < width_; x++) {
    for (int y = 0; y < height_; y++) {
      this->setPixel(x, y, img.pixel(x, y));
    }
  }
}

CaptchaImage::CaptchaImage(const CaptchaImage& r) {
  width_ = r.width_;
  height_= r.height_;
  size_ = width_ * height_;
  buf_ = new QRgb[size_];
  for (int x = 0; x < width_; x++) {
    for (int y = 0; y < height_; y++) {
      this->setPixel(x, y, r.pixel(x, y));
    }
  }
}

CaptchaImage::~CaptchaImage(){
  if (buf_ != nullptr) {
    delete[] buf_;
    buf_ = nullptr;
  }
  width_ = 0;
  height_ = 0;
  size_ = 0;
}

void CaptchaImage::save(QString fileName) {
  QImage img(width_, height_, QImage::Format_RGB32);
  for (int x = 0; x < width_; x++) {
    for (int y = 0; y < height_; y++) {
      img.setPixel(x, y, this->pixel(x, y));
    }
  }
  img.save(fileName);
}

Captcha::Captcha()
{
  load("0.png", '0');
  load("1.png", '1');
  load("2.png", '2');
  load("3.png", '3');
  load("4.png", '4');
  load("5.png", '5');
  load("6.png", '6');
  load("7.png", '7');
  load("8.png", '8');
  load("9.png", '9');
  load("minus.png", '-');
  load("multiply.png", '*');
  load("plus.png", '+');
}

QString Captcha::solve(CaptchaImage& target) {
  inverse(target);
  QString res;
  int width = target.width();
  int height = target.height();
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      QChar c = solve(x, y, target);
      if (c != QChar('\0')) {
        res += c;
        static int cnt = 0;
        // target.save("temp/" + QString::number(cnt) + ".png");
        cnt++;
      }
    }
  }

  qDebug() << QString("solve return %1").arg(res);
  return res;
}

QChar Captcha::solve(int x, int y, CaptchaImage& target) {
  // qDebug() << x << y; // gilgil temp
  for (int idx = 0; idx < ccList_.count(); idx++) {
    const CaptchaImage& img = ccList_.at(idx).img_;
    bool rOk = check(img, x, y, R, target);
    bool gOk = check(img, x, y, G, target);
    bool bOk = check(img, x, y, B, target);
    if (!rOk && !bOk && !gOk) continue;
    if (rOk && gOk && !bOk) {
      sub(img, x, y, R, target);
      sub(img, x, y, G, target);
      return ccList_.at(idx).ch_;
    } else
    if (rOk && !gOk && bOk) {
      sub(img, x, y, R, target);
      sub(img, x, y, B, target);
      return ccList_.at(idx).ch_;
    } else
    if (!rOk && gOk && bOk) {
      sub(img, x, y, G, target);
      sub(img, x, y, B, target);
      return ccList_.at(idx).ch_;
    }
  }
  return QChar('\0');
}

void Captcha::load(QString fileName, QChar ch) {
  QPixmap pm;
  if (!pm.load("char/" + fileName)) {
    qCritical() << QString("pm.load(%1) return false").arg(fileName);
    return;
  }

  CaptchaChar cc;
  QImage img = pm.toImage();
  int width = img.width();
  int height = img.height();
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      QRgb rgb = img.pixel(x, y);
      int r = qRed(rgb);
      int g = qGreen(rgb);
      int b = qBlue(rgb);
      int m = max(r, g, b);
      img.setPixel(x, y, qRgb(m, m, m));
    }
    // img.save("char/" + fileName + ".png"); // gilgil temp
  }
  cc.img_.load(img);
  cc.ch_ = ch;
  ccList_.push_back(cc);
}

int Captcha::max(int a, int b, int c) {
  int res = a;
  if (res < b) res = b;
  if (res < c) res = c;
  return res;
}

bool Captcha::check(const CaptchaImage& img, int x, int y, CaptchaColor c, CaptchaImage& target) {
  int imgWidth = img.width();
  int imgHeight = img.height();
  int targetWidth = target.width();
  int targetHeight = target.height();
  if (x + imgWidth > targetWidth) return false;
  if (y + imgHeight > targetHeight) return false;
  int failCount = 0;
  for (int yy = 0; yy < imgHeight; yy++) {
    for (int xx = 0; xx < imgWidth ; xx++) {
      int maskColor = qRed(img.pixel(xx, yy));
      if (maskColor == 0)
        continue;
      int targetColor = R;
      switch (c) {
      case R: targetColor = qRed  (target.pixel(x + xx, y + yy)); break;
      case G: targetColor = qGreen(target.pixel(x + xx, y + yy)); break;
      case B: targetColor = qBlue (target.pixel(x + xx, y + yy)); break;
      }
      if ((float)maskColor * .9 > (float)targetColor) {
        if (failCount++ > 10)
          return false;
      }
    }
  }
  return true;
}

void Captcha::sub(const CaptchaImage& img, int x, int y, CaptchaColor c, CaptchaImage& target) {
  int imgWidth = img.width();
  int imgHeight = img.height();
  int targetWidth = target.width();
  int targetHeight = target.height();
  Q_ASSERT(x + imgWidth <= targetWidth);
  Q_ASSERT(y + imgHeight <= targetHeight);
  for (int xx = 0; xx < imgWidth ; xx++) {
    for (int yy = 0; yy < imgHeight; yy++) {
      int maskColor = qRed(img.pixel(xx, yy));
      if (maskColor == 0)
        continue;
      QRgb targetColor = target.pixel(x + xx, y + yy);
      QRgb newColor = qRgb(0, 0, 0);
      switch (c) {
      case R: newColor = qRgb(qRed(targetColor) - maskColor, qGreen(targetColor), qBlue(targetColor)); break;
      case G: newColor = qRgb(qRed(targetColor), qGreen(targetColor) - maskColor, qBlue(targetColor)); break;
      case B: newColor = qRgb(qRed(targetColor), qGreen(targetColor), qBlue(targetColor) - maskColor); break;
      }
      target.setPixel(x + xx, y + yy, newColor);
    }
  }
}

void Captcha::inverse(CaptchaImage& target) {
  int width = target.width();
  int height = target.height();
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      QRgb rgb = target.pixel(x, y);
      rgb = (~rgb) & RGB_MASK;
      target.setPixel(x, y, rgb);
    }
  }
}
