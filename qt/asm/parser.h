#pragma once

#include <QDebug>
#include <QString>
#include "bigint/BigInteger.hh"
#include "bigint/BigIntegerUtils.hh"

struct Parser {
  Parser();
  BigInteger calculate(QString s);
};
