#include <QApplication>
#include <QDebug>
#include <QPixmap>
#include <QRgb>

int max(int a, int b, int c) {
  int res = a;
  if (res < b) res = b;
  if (res < c) res = c;
  return res;
}

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  QPixmap pm;
  qDebug() << pm.load("char/4.png");
  QImage img = pm.toImage();

  int width = img.width();
  int height = img.height();


  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      QRgb rgb = img.pixel(x, y);
      int r = qRed(rgb);
      int g = qGreen(rgb);
      int b = qBlue(rgb);
      int m = max(r, g, b);
      if (m != 0)
        qDebug() << x << y << m;
    }
  }
  return 0;
}
