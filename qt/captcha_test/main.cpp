#include <iostream>
#include <QApplication>
#include <QDebug>
#include <QPixmap>
#include <QImage>

#include "../asm/captcha.h"
#include "../asm/parser.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  QPixmap map;

  map.load("/root/sctf/asm/pcap/asm9/04d03fb2d138564ac39ff9c0c0d87b3a.png"); // 56*87         > 56*8
  //map.load("/root/sctf/asm/pcap/asm9/5f0ab8048f15f2b628fefee6b30e162b.png"); // 421+374       > 44
  //map.load("/root/sctf/asm/pcap/asm9/6a44b32e73c577616a4815f6e5d35931.png"); // 314815+125601 > 314815+125601
  //map.load("/root/sctf/asm/pcap/asm9/24af455ed4627b4410095e77c8c965be.png"); // 568-314       > 568-314
  //map.load("/root/sctf/asm/pcap/asm9/40b238ca7b6fdfba7dd610ad8e72f740.png"); // 6-0           > 6-0
  //map.load("/root/sctf/asm/pcap/asm9/44b3498c79772c7b58384cd9aeacdfbb.png"); // 58826x44659   > 58826x44659
  //map.load("/root/sctf/asm/pcap/asm9/b7c21d050b8f399d220b94ac2ccb5eb0.png"); // 2489+1111     > 2489+1111
  //map.load("/root/sctf/asm/pcap/asm9/b68bb4c6c885ee44682dab02f9ad2313.png"); // 63*16         > 63*16
  //map.load("/root/sctf/asm/pcap/asm9/c246015b3fcbd81859ac71a0fba60319.png"); // 2263+7240     > 2263+7240
  //map.load("/root/temp/e2af8b77e113db7dc515e760d6c08227.png");
  CaptchaImage img;
  img.load(map.toImage());

  Captcha c;
  QString s = c.solve(img);

  Parser p;
  BigInteger ans = p.calculate(s);
  bigIntegerToString(ans);
  std::string _s = bigIntegerToString(ans);
  std::clog << "calculate " << _s << std::endl;

  return 0;
}
